const Apify = require('apify');
const cheerio = require('cheerio');
const urlZomato = 'https://www.zomato.com';
const urlWilayah = urlZomato + '/jakarta/';
const urlType = urlWilayah + 'restaurants';

// Apify.main is a helper function, you don't need to use it.
Apify.main(async () => {
    const requestQueue = await Apify.openRequestQueue();
    // Choose the first URL to open.
    await requestQueue.addRequest({ url: urlType + '?bar=1' });
    await requestQueue.addRequest({ url: urlType });


    const zomatoStore = await Apify.openKeyValueStore('zomato-store');

    const crawler = new Apify.PlaywrightCrawler({
        maxConcurrency: 1,
        requestQueue,
        handlePageFunction: async ({ request, response, page }) => {

            // Extract HTML title of the page.
            const title = await page.title();
            const url = request.url;

            const isAlcohol = url.indexOf('?bar=1') != -1;
            const content = await response.text();
            // console.log(aing);

            const $ = cheerio.load(content);
            let zomatoData = await zomatoStore.getValue('list');
            if (zomatoData === null) {
                zomatoData = {};
            }
            $("#orig-search-list .search-card").each(function (object, index) {
                const cost = $('.res-cost span.pl0', this).text().trim();
                const title = $('.result-title', this).text().trim();
                const url = $('a.result-title', this).attr('href').trim();
                const alamat = $('a.search_result_subzone', this).text().trim();
                const alamatlengkap = $('div.search-result-address', this).text().trim();
                const key = url.replace(urlWilayah, '');

                const dataItem = {
                    title,
                    cost,
                    url,
                    alamat,
                    alamatlengkap,
                    alcohol: isAlcohol,
                };
                if (zomatoData.hasOwnProperty(key) && zomatoData[key].alcohol === true) {

                } else {
                    zomatoData[key] = dataItem
                }

            })
            zomatoStore.setValue('list', zomatoData);

            $(".search-pagination-top .paginator_item").each(async function () {
                const urlNextPage = $(this).attr('href');
                if (typeof urlNextPage != 'undefined') {
                    await requestQueue.addRequest({ url: urlZomato + urlNextPage });
                }
            });
        },
    });

    await crawler.run();
});